library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
 
entity CLK_DIV is
     port( 
          clk  :in  std_logic;
          clk2 :out std_logic
     );
end CLK_DIV;

architecture behavioral of CLK_DIV is
     signal slow_count: integer range 0 to 40001 := 0;
begin
		
	clk2_process: process(clk)
	begin
		if (rising_edge(clk)) then
			slow_count <= slow_count + 1;
			if (slow_count = 40000) then
				slow_count <= 0;
				clk2 <= '0';
			elsif (slow_count = 20000) then
				clk2 <= '1';
			end if;
		end if;
	end process clk2_process;

end behavioral; 
