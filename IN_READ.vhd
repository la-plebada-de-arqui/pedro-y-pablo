library ieee;
use ieee.std_logic_1164.all;

entity IN_READ is 
     port( 
          clk            :in  std_logic;  --system clock
          --UART
          i_RX_Serial    :in  std_logic;
          o_RX_DV        :out std_logic;
          --SIGNALS
          WORD           :out std_logic_vector(23 downto 0);
          DONE           :out std_logic
     ); 
end IN_READ;

architecture behavioral of IN_READ is
     
     component UART_RX is
     port (
          i_Clk       : in  std_logic;
          i_RX_Serial : in  std_logic;
          o_RX_DV     : out std_logic;
          o_RX_Byte   : out std_logic_vector(7 downto 0)
          );
     end component;

     --Signals
     signal flag         :std_logic;
     signal data         :std_logic_vector(7 downto 0);
     signal tmp_word     :std_logic_vector(23 downto 0);
begin
     --UART to Signals
     U2S: UART_RX port map(clk,i_RX_Serial,flag,data);
     process(flag,clk)
          variable char  :  integer range 0 to 6 := 0;
     begin
          if(clk'event and clk='1') then
			 if(flag='1') then 
               if(char < 7) then
                    DONE <= '0';
                    char := char + 1;
               end if;
               case char is
                    when 1 => WORD(3 downto 0)   <= data(3 downto 0);
                    when 2 => WORD(7 downto 4)   <= data(3 downto 0);
                    when 3 => WORD(11 downto 8)  <= data(3 downto 0);
                    when 4 => WORD(15 downto 12) <= data(3 downto 0);
                    when 5 => WORD(19 downto 16) <= data(3 downto 0);
                    when 6 => WORD(23 downto 20) <= data(3 downto 0);
                    when others => 
                         DONE <= '1';
                         char:=0;
               end case;
          end if;
			 end if;
     end process;
  
End behavioral; 
