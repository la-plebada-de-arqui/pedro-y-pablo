library ieee;
use ieee.std_logic_1164.all;

entity LCD is
     port(
          clk       : in  std_logic;  --system clock
          rw        : out std_logic;  --read/write
          rs        : out std_logic;  --setup/data
          e         : out std_logic;  --enable 
          flag      : in  std_logic;
          data_in   : in std_logic_vector(23 downto 0); --data in
          lcd_data  : out std_logic_vector(7 downto 0)); --data signals for lcd
end LCD;

architecture behavior of LCD is
  signal   lcd_enable : std_logic;
  signal   lcd_bus    : std_logic_vector(9 downto 0);
  signal   lcd_busy   : std_logic;
  component lcd_controller is
    port(
       clk        : in  std_logic; --system clock
       reset_n    : in  std_logic; --active low reinitializes lcd
       lcd_enable : in  std_logic; --latches data into lcd controller
       lcd_bus    : in  std_logic_vector(9 downto 0); --data and control signals
       busy       : out std_logic; --lcd controller busy/idle feedback
       rw, rs, e  : out std_logic; --read/write, setup/data, and enable for lcd
       lcd_data   : out std_logic_vector(7 downto 0)); --data signals for lcd
  end component;
begin

  --instantiate the lcd controller
  dut: lcd_controller
    port map(clk => clk, reset_n => '1', lcd_enable => lcd_enable, lcd_bus => lcd_bus, 
             busy => lcd_busy, rw => rw, rs => rs, e => e, lcd_data => lcd_data);
  

     process(clk,flag)
          variable char  :  integer range 0 to 8 := 0;
     begin
          if(clk'event and clk = '1') then
          if(flag='1') then
               if(lcd_busy = '0' and lcd_enable = '0') then
                    lcd_enable <= '1';
                    if(char < 8) then
                         char := char + 1;
                    end if;
                    case char is
                         when 1 => lcd_bus <= "100011"&data_in(3  downto 0 );
                         when 2 => lcd_bus <= "100011"&data_in(7  downto 4 );
                         when 3 => lcd_bus <= "100011"&data_in(11 downto 8 );
                         when 4 => lcd_bus <= "100011"&data_in(15 downto 12);
                         when 5 => lcd_bus <= "100011"&data_in(19 downto 16);
                         when 6 => lcd_bus <= "100011"&data_in(23 downto 20);
                         when 7 => lcd_bus <= "1000010111";
								 when others => 
                              lcd_enable <= '0';
                    end case;
               else
                    lcd_enable <= '0';
               end if;
		elsif(flag='0') then
			char := 0;
          end if;
          end if;
  end process;
 
end behavior;
