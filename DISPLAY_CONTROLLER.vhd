library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
 
entity DISPLAY_CONTROLLER is
     port( 
          clk  :in  std_logic;
          cs   :in  std_logic;
          rw   :in  std_logic;
          addr :in  std_logic_vector(1 downto 0);
          data :in  std_logic_vector(7 downto 0);
          --Display
          y    :out std_logic_vector(7 downto 0);
          anode:out std_logic_vector(3 downto 0)
     );
end DISPLAY_CONTROLLER;

architecture behavioral of DISPLAY_CONTROLLER is
     signal char1: std_logic_vector(3 downto 0);
	signal char2: std_logic_vector(3 downto 0);
	signal seg1: std_logic_vector(7 downto 0);
	signal seg2: std_logic_vector(7 downto 0);
	signal displaycounter: integer range 0 to 3;
begin
	-- display driver

	char_process: process(data)
	begin
	     char1 <= data(7 downto 4);
		char2 <= data(3 downto 0);
	end process char_process;

	--display asigna el vector de 8 bits que se manda a los display
	with char1 select
		seg1 <=   "00000011" when x"0",
	               "10011111" when x"1",
			     "00100101" when x"2",
				"00001101" when x"3",
				"10011001" when x"4",
				"01001001" when x"5",
				"01000001" when x"6",
				"00011111" when x"7",
				"00000001" when x"8",
				"00001001" when x"9",
				"00010001" when x"A",
				"11000001" when x"b",
				"01100011" when x"C",
				"10000101" when x"d",
				"01100001" when x"E",
				"01110001" when x"F",
				"11111111" when others;

	with char2 select
		seg2 <=   "00000011" when x"0",
				"10011111" when x"1",
				"00100101" when x"2",
				"00001101" when x"3",
				"10011001" when x"4",
				"01001001" when x"5",
				"01000001" when x"6",
				"00011111" when x"7",
				"00000001" when x"8",
				"00001001" when x"9",
				"00010001" when x"A",
				"11000001" when x"b",
				"01100011" when x"C",
				"10000101" when x"d",
				"01100001" when x"E",
				"01110001" when x"F",
				"11111111" when others;
		
     --displaying multiplexa entre displays
     displaying: process(seg1, seg2)
	begin
		if (rising_edge(clk)) then
			if (displaycounter = 1) then
			     displaycounter <= 0;
			else
				displaycounter <= displaycounter + 1;
			end if;
		end if;
		case(displaycounter) is
			when 0 =>
				Y <= seg1;
				anode <= "0111";
				
			when 1 =>	
				Y <= seg2;
				anode <= "1011";
				
			when others =>	
				Y <= "11111111";
				anode <= "1111";
		end case;	
	end process displaying;     
 
end behavioral; 
