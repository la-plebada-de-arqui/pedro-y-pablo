--------------------------------------------------------------------------------
--
--   filename:         lcd_controller.vhd
--   dependencies:     none
--   design software:  quartus ii 32-bit version 11.1 build 173 sj full version
--
--   hdl code is provided "as is."  digi-key expressly disclaims any
--   warranty of any kind, whether express or implied, including but not
--   limited to, the implied warranties of merchantability, fitness for a
--   particular purpose, or non-infringement. in no event shall digi-key
--   be liable for any incidental, special, indirect or consequential
--   damages, lost profits or lost data, harm to your equipment, cost of
--   procurement of substitute goods, technology or services, any claims
--   by third parties (including but not limited to any defense thereof),
--   any claims for indemnity or contribution, or other similar costs.
--
--   version history
--   version 1.0 6/2/2006 scott larson
--     initial public release
--    version 2.0 6/13/2012 scott larson
--
--   clock frequency: to change system clock frequency, change line 65
--
--   lcd initialization settings: to change, comment/uncomment lines:
--
--   function set  
--      2-line mode, display on             line 93    lcd_data <= "00111100";
--      1-line mode, display on             line 94    lcd_data <= "00110100";
--      1-line mode, display off            line 95    lcd_data <= "00110000";
--      2-line mode, display off            line 96    lcd_data <= "00111000";
--   display on/off
--      display on, cursor off, blink off   line 104   lcd_data <= "00001100";
--      display on, cursor off, blink on    line 105   lcd_data <= "00001101";
--      display on, cursor on, blink off    line 106   lcd_data <= "00001110";
--      display on, cursor on, blink on     line 107   lcd_data <= "00001111";
--      display off, cursor off, blink off  line 108   lcd_data <= "00001000";
--      display off, cursor off, blink on   line 109   lcd_data <= "00001001";
--      display off, cursor on, blink off   line 110   lcd_data <= "00001010";
--      display off, cursor on, blink on    line 111   lcd_data <= "00001011";
--   entry mode set
--      increment mode, entire shift off    line 127   lcd_data <= "00000110";
--      increment mode, entire shift on     line 128   lcd_data <= "00000111";
--      decrement mode, entire shift off    line 129   lcd_data <= "00000100";
--      decrement mode, entire shift on     line 130   lcd_data <= "00000101";
--    
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity lcd_controller is
  port(
    clk        : in    std_logic;  --system clock
    reset_n    : in    std_logic;  --active low reinitializes lcd
    lcd_enable : in    std_logic;  --latches data into lcd controller
    lcd_bus    : in    std_logic_vector(9 downto 0);  --data and control signals
    busy       : out   std_logic := '1';  --lcd controller busy/idle feedback
    rw, rs, e  : out   std_logic;  --read/write, setup/data, and enable for lcd
    lcd_data   : out   std_logic_vector(7 downto 0)); --data signals for lcd
end lcd_controller;

architecture controller of lcd_controller is
  type control is(power_up, initialize, ready, send);
  signal    state      : control;
  constant  freq       : integer := 50; --system clock frequency in mhz
begin
  process(clk)
    variable clk_count : integer := 0; --event counter for timing
  begin
  if(clk'event and clk = '1') then
    
      case state is
        
        --wait 50 ms to ensure vdd has risen and required lcd wait is met
        when power_up =>
          busy <= '1';
          if(clk_count < (50000 * freq)) then    --wait 50 ms
            clk_count := clk_count + 1;
            state <= power_up;
          else                                   --power-up complete
            clk_count := 0;
            rs <= '0';
            rw <= '0';
            lcd_data <= "00110000";
            state <= initialize;
          end if;
          
        --cycle through initialization sequence  
        when initialize =>
          busy <= '1';
          clk_count := clk_count + 1;
          if(clk_count < (10 * freq)) then       --function set
            lcd_data <= "00111100";      --2-line mode, display on
            --lcd_data <= "00110100";    --1-line mode, display on
            --lcd_data <= "00110000";    --1-line mdoe, display off
            --lcd_data <= "00111000";    --2-line mode, display off
            e <= '1';
            state <= initialize;
          elsif(clk_count < (60 * freq)) then    --wait 50 us
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          elsif(clk_count < (70 * freq)) then    --display on/off control
            lcd_data <= "00001100";      --display on, cursor off, blink off
            --lcd_data <= "00001101";    --display on, cursor off, blink on
            --lcd_data <= "00001110";    --display on, cursor on, blink off
            --lcd_data <= "00001111";    --display on, cursor on, blink on
            --lcd_data <= "00001000";    --display off, cursor off, blink off
            --lcd_data <= "00001001";    --display off, cursor off, blink on
            --lcd_data <= "00001010";    --display off, cursor on, blink off
            --lcd_data <= "00001011";    --display off, cursor on, blink on            
            e <= '1';
            state <= initialize;
          elsif(clk_count < (120 * freq)) then   --wait 50 us
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          elsif(clk_count < (130 * freq)) then   --display clear
            lcd_data <= "00000001";
            e <= '1';
            state <= initialize;
          elsif(clk_count < (2130 * freq)) then  --wait 2 ms
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          elsif(clk_count < (2140 * freq)) then  --entry mode set
            --lcd_data <= "00000110";      --increment mode, entire shift off
            --lcd_data <= "00000111";    --increment mode, entire shift on
            --lcd_data <= "00000100";    --decrement mode, entire shift off
            lcd_data <= "00000101";    --decrement mode, entire shift on
            e <= '1';
            state <= initialize;
          elsif(clk_count < (2200 * freq)) then  --wait 60 us
            lcd_data <= "00000000";
            e <= '0';
            state <= initialize;
          else                                   --initialization complete
            clk_count := 0;
            busy <= '0';
            state <= ready;
          end if;    
       
        --wait for the enable signal and then latch in the instruction
        when ready =>
          if(lcd_enable = '1') then
            busy <= '1';
            rs <= lcd_bus(9);
            rw <= lcd_bus(8);
            lcd_data <= lcd_bus(7 downto 0);
            clk_count := 0;            
            state <= send;
          else
            busy <= '0';
            rs <= '0';
            rw <= '0';
            lcd_data <= "00000000";
            clk_count := 0;
            state <= ready;
          end if;
        
        --send instruction to lcd        
        when send =>
        busy <= '1';
        if(clk_count < (50 * freq)) then  --do not exit for 50us
           busy <= '1';
           if(clk_count < freq) then      --negative enable
            e <= '0';
           elsif(clk_count < (14 * freq)) then  --positive enable half-cycle
            e <= '1';
           elsif(clk_count < (27 * freq)) then  --negative enable half-cycle
            e <= '0';
           end if;
           clk_count := clk_count + 1;
           state <= send;
        else
          clk_count := 0;
          state <= ready;
        end if;

      end case;    
    
      --reset
      if(reset_n = '0') then
          state <= power_up;
      end if;
    
    end if;
  end process;
end controller;
