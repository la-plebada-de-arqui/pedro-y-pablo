library ieee;
use ieee.std_logic_1164.all;

entity TheBox_1 is 
     port( 
          clk       : in  std_logic;  --system clock
          --LCD
          rw        : out std_logic;  --read/write
          rs        : out std_logic;  --setup/data
          e         : out std_logic;  --enable 
          lcd_data  : out std_logic_vector(7 downto 0); --data signals for lcd
          --UART
          i_RX_Serial : in  std_logic;
          o_RX_DV     : out std_logic
     ); 
end TheBox_1;

architecture behavioral of TheBox_1 is
     
     component LCD is
     port(
          clk       : in  std_logic;  --system clock
          rw        : out std_logic;  --read/write
          rs        : out std_logic;  --setup/data
          e         : out std_logic;  --enable 
          data_in   : in std_logic_vector(7 downto 0); --data in
          lcd_data  : out std_logic_vector(7 downto 0)
          );
     end component;

     component UART_RX is
     port (
          i_Clk       : in  std_logic;
          i_RX_Serial : in  std_logic;
          o_RX_DV     : out std_logic;
          o_RX_Byte   : out std_logic_vector(7 downto 0)
          );
     end component;

     --Signals
     signal data         :std_logic_vector(7 downto 0);

begin
     --UART to Signals
     U2S: UART_RX port map(clk,i_RX_Serial,o_RX_DV,data);
     --Signals to LCD
     S2L: LCD port map(clk,rw,rs,e,data,lcd_data);
end behavioral; 
