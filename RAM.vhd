library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
 
entity RAM is
     port( 
          cs      :in    std_logic;
          rw      :in    std_logic;
          addr    :in    std_logic_vector(7 downto 0);
          data    :inout std_logic_vector(23 downto 0)
     );
end RAM;

architecture behavioral of RAM is
     type mem_array is array(0 to 255) of std_logic_vector(23 downto 0);
     signal tmp_data: std_logic_vector(23 downto 0);
     signal mem: mem_array;
begin
     
     read: process(cs,rw,addr)
     begin
          if(rw='1') then
               mem(conv_integer(addr))<=data;
          end if;
     end process read;
     
     write: process(cs,rw,addr)
     begin
          if(rw='0') then
               tmp_data<=mem(conv_integer(addr));
          end if;
     end process write;
     
     data <= tmp_data when cs = '0' else "ZZZZZZZZZZZZZZZZZZZZZZZZ";
 
end behavioral; 
