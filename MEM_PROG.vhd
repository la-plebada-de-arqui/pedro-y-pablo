library ieee;
use ieee.std_logic_1164.all;

entity IN_READ is 
     port( 
          clk            :in  std_logic;  --system clock
          --MEM
          mem            :out std_logic_vector(239 downto 0);
          ready          :out std_logic;
          --SIGNALS
          WORD           :in std_logic_vector(23 downto 0);
          DONE           :in std_logic
     ); 
end IN_READ;

architecture behavioral of IN_READ is
     
begin
     --STORE WORD
     process(DONE,clk)
          variable flag  :integer range 0 to 11 := 0;
     begin
          if(clk'event and clk='1') then
			if(DONE='1') then 
                    if(flag < 11) then
                         ready <= '0';
                         flag:= flag + 1;
                    end if;
                    case flag is
                         when 1  => mem(23  downto 0  ) <= WORD;
                         when 2  => mem(47  downto 24 ) <= WORD;
                         when 3  => mem(71  downto 48 ) <= WORD;
                         when 4  => mem(95  downto 72 ) <= WORD;
                         when 5  => mem(119 downto 96 ) <= WORD;
                         when 6  => mem(143 downto 120) <= WORD;
                         when 7  => mem(167 downto 144) <= WORD;
                         when 8  => mem(191 downto 168) <= WORD;
                         when 9  => mem(215 downto 192) <= WORD;
                         when 10 => mem(239 downto 216) <= WORD;
                         when others => 
                              ready <= '1';
                    end case;
               else
                    flag:=0;
               end if;
		end if;
     end process;
  
end behavioral; 
