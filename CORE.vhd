library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CORE is
     port
	(
	     clock     :in    std_logic; 
          --Control_Signals
          reset     :in    std_logic; 
          run       :in    std_logic; --PorImplementar
		regin     :in    std_logic_vector(15 downto 0); --PorEliminar
          prog_in   :in    std_logic_vector(239 downto 0);
	     --Memory
          data_ibus :inout std_logic_vector(23 downto 0);
          addr_ibus :out   std_logic_vector(7 downto 0);
		cs        :out   std_logic;
          rw        :out   std_logic;
          --I/O
          data_ebus :inout std_logic_vector(15 downto 0);
          addr_ebus :out   std_logic_vector(1 downto 0);
		cs_e      :out   std_logic;
          rw_e      :out   std_logic
	);
end entity CORE;

architecture behavioral of CORE is
     
     --Instruction Memory 24bits
     --Register File 16bits
     type IM_array is array(0 to 255) of std_logic_vector(23 downto 0);
	type RF_array is array(0 to 15) of std_logic_vector(15 downto 0);

	signal IM: IM_array;
	signal PC: integer range 0 to 255 := 0;
	signal PC_temp: integer range 0 to 255;
	signal PC_return: std_logic; --Quizá se puede optimizar esto
	signal IR: std_logic_vector(23 downto 0);
	signal opcode: std_logic_vector (3 downto 0);
	signal RA: std_logic_vector(7 downto 0);
	signal RB: std_logic_vector(7 downto 0);
	signal RD: std_logic_vector(3 downto 0);
	signal RF: RF_array;
	signal immediate: std_logic_vector(15 downto 0);
	signal W: std_logic_vector(15 downto 0);
	signal terminate: std_logic;

     signal PC_deb_count: integer range 0 to 100; --Puedo ponerlo a parte como component
	signal mux_deb_count: integer range 0 to 100;
	signal PC_pulse: std_logic;

	signal slow_clock: std_logic; --Lo puedo cambiar para que sea directo o usar otro in
	signal slow_count: integer range 0 to 20001 := 0;

	begin
	-- processor stages (t0, t1, t2, t3)
          
     fetch: process(slow_clock, reset)
	begin
	     if (reset = '0') then --Aquí se programa
               IM(0) <= prog_in(23  downto 0); 
               IM(1) <= prog_in(47  downto 24);
               IM(2) <= prog_in(71  downto 48);
               IM(3) <= prog_in(95  downto 72);
               IM(4) <= prog_in(119 downto 96);
               IM(5) <= prog_in(143 downto 120);
               IM(6) <= prog_in(167 downto 144);
               IM(7) <= prog_in(191 downto 168);
               IM(8) <= prog_in(215 downto 192);
               IM(9) <= prog_in(239 downto 216);
               IM(10 to 255) <= (others => x"000000");
          elsif rising_edge(slow_clock) then
			IR <= IM(PC);
		end if;
	end process fetch;

	decode: process(slow_clock, reset)
	begin
	     if (reset = '0') then
			opcode <= "0000";
			RA <= "00000000";
			RB <= "00000000";
			RD <= "0000";
			immediate <= x"0000";
		elsif rising_edge(slow_clock) then
			opcode <= IR(23 downto 20);
			RA <= IR(19 downto 12);
			RB <= IR(11 downto 4);
			RD <= IR(3 downto 0);
			immediate <= IR(19 downto 4);
		end if;
	end process decode;

	execute: process(slow_clock, reset)
	begin
		if (reset = '0') then
			W <= x"0000";
			terminate <= '0';
		elsif rising_edge(slow_clock) then
			case(opcode) is
			--Main
				when "0000" => -- HALT
					terminate <= '1';
					W <= W;
                         
                    when "0001" => -- LD
                         W <= RF(conv_integer(RA));
                         
                    when "0010" => -- LDI
					W <= immediate;
					
                    when "0011" => -- JMP
					W <= immediate;

			--Logic
                    when "0100" => -- AND
					W <= RF(conv_integer(RA)) AND RF(conv_integer(RB));

                    when "0101" => -- OR
					W <= RF(conv_integer(RA)) OR RF(conv_integer(RB));

				when "0110" => -- NOT
					W <= NOT RF(conv_integer(RA));
					
                    when "0111" => -- XOR
					W <= RF(conv_integer(RA)) XOR RF(conv_integer(RB));

               --Arith
                    when "1000" => -- ADD
					W <= RF(conv_integer(RA)) + RF(conv_integer(RB));

				when "1001" => -- SUB
					W <= RF(conv_integer(RA)) - RF(conv_integer(RB));
					
                    when "1010" => -- MUL
					W <= RF(conv_integer(RA))(15 downto 8) * RF(conv_integer(RB))(15 downto 8);
					
                    when "1011" => -- DIV
					W <= std_logic_vector(to_unsigned(to_integer(unsigned(RF(conv_integer(RA))) / unsigned(RF(conv_integer(RB)))),16));
                         
               --Register
                    when "1100" => -- LSL
                         W <= std_logic_vector(shift_left(unsigned(RF(conv_integer(RA))),conv_integer(RB)));
                    when "1101" => -- LSR
                         W <= std_logic_vector(shift_right(unsigned(RF(conv_integer(RA))),conv_integer(RB)));
               --I/O
                    when "1110" => -- IN
                         W <= x"FFFF";
                    when "1111" => -- OUT
                         W <= x"FFFF";
			--Default
				when others =>
					W <= x"FFFF";	
			end case;
		end if;
	end process execute;

	store: process(slow_clock, reset)
	begin
		if (reset = '0') then
			RF(0 to 15) <= (others => x"0000");
		elsif rising_edge(slow_clock) then
			RF(conv_integer(RD)) <= W;
               RF(14) <= regin;
		end if;
     end process store;
		
	PC_counter: process(PC_pulse)
	begin
	  	if (reset = '0') then
	  		PC <= 0;
	  	elsif (terminate = '1') then
	  		PC <= PC;
	    	elsif rising_edge(PC_pulse) then
			if (opcode = "0011") then
				PC_temp <= PC;
				PC <= conv_integer(immediate);
				PC_return <= '1';
			elsif (PC_return = '1') then
				PC <= PC_temp + 1;
				PC_return <= '0';
			else
				PC <= PC + 1;
			end if;
		end if;
	end process PC_counter;
		
		
		
	-- clock y PC
		
	--"slow clock" Para de debounce 
	slow_clock_process: process(clock)
	begin
		if (rising_edge(clock)) then
			slow_count <= slow_count + 1;
			if (slow_count = 20000) then
				slow_count <= 0;
				slow_clock <= '0';
			elsif (slow_count = 10000) then
				slow_clock <= '1';
			end if;
		end if;
	end process slow_clock_process;


end architecture behavioral;
