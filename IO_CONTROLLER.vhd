library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity IO_CONTROLLER is
     port( 
          --Internal_Signals
          cs      :in    std_logic;
          rw      :in    std_logic;
          addr    :in    std_logic_vector(7 downto 0);
          data    :inout std_logic_vector(15 downto 0);
          --External_Signals
          input   :in    std_logic_vector(15 downto 0);
          output  :out   std_logic_vector(15 downto 0)
     );

end IO_CONTROLLER;

architecture behavioral of IO_CONTROLLER is
     signal tmp_data: std_logic_vector(15 downto 0);
     signal tmp_in  : std_logic_vector(15 downto 0);
     signal tmp_out : std_logic_vector(15 downto 0);
begin
     
     IO_OUT: process(cs,rw,addr)
     begin
          if(rw='1') then
               output<=data;
          end if;
     end process IO_OUT;
     
     IO_IN: process(cs,rw,addr)
     begin
          if(rw='0') then
               tmp_data<=input;
          end if;
     end process IO_IN;
     
     data <= tmp_data when cs = '0' else "ZZZZZZZZZZZZZZZZ";
 
end behavioral; 
